# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.3.0] - 20-12-2019

### Added
* A test to be protected against required change tune with Nan!

## [3.2.0] - 04-07-2019

### Changed
* Update to the new name of the all quad device attribute
(from DisabledMagnets to FrozenMagnets)

## [3.1.0] - 16-05-2019

### Added
* Add a check in the SetTune command that a tune measurement has been done
between two consecutive SetTune command

## [3.0.0] - 12-04-2019

* Cleanup the code to support only EBS (Former SR does not exist any more)

### Changed
* The ComputationMatrix attribute is now read only

### Added
* The device subscribe to "all" magnets device Disable(Frozen) magnets attribute
in order to re-compute its matrix when one magnet is frozen (disabled)

## [2.0.1] - 28-08-2018

### Changed
* For EBS: Adapted to the new definition of the all quadrupoles *MagnetNames* attribute

## [2.0.0] - 30-05-2018

First ESRF official release

### Added
* Add ESRF official README and CHANGELOG files

### Changed
* Commented out tests which may change something on real SR
