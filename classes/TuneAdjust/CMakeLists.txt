###############################################################################
project(TuneAdjust
    VERSION 
        ${_VERSION}
    DESCRIPTION
        "Tune Adjust"
    LANGUAGES
        CXX
)


# Tango Class dependencies (inheritance from parent Tango Class) declaration
set(LINK_LIBRARIES_CLASS
)
###############################################################################
# Build library
include(build-tango-class)
