/*----- PROTECTED REGION ID(TuneAdjust.h) ENABLED START -----*/
//=============================================================================
//
// file :        TuneAdjust.h
//
// description : Include file for the TuneAdjust class
//
// project :     Tune Adjust
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
//
// Copyright (C): 2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef TuneAdjust_H
#define TuneAdjust_H

#include <chrono>


/*----- PROTECTED REGION END -----*/	//	TuneAdjust.h
#include <tango/tango.h>


#ifdef TANGO_LOG
	// cppTango after c934adea (Merge branch 'remove-cout-definition' into 'main', 2022-05-23)
	// nothing to do
#else
	// cppTango 9.3-backports and older
	#define TANGO_LOG       cout
	#define TANGO_LOG_INFO  cout2
	#define TANGO_LOG_DEBUG cout3
#endif // TANGO_LOG

/**
 *  TuneAdjust class description:
 *    Tango class to 
 *    1 - change the accelerator (SR or EBS) tunes to a
 *    set of desired values. This class applied a computed delta I
 *    on the accelerator quadrupoles.
 *    2 - provide a mux for the official tune values from the different
 *    available tune measurement(s) system
 */


namespace TuneAdjust_ns
{
enum class _Source_QhEnum : short {
	_HminusSYSTEM1,
	_HminusSYSTEM2,
	_HminusSYSTEM3,
	_HminusSYSTEM4,
	_HminusSYSTEM5,
} ;
typedef _Source_QhEnum Source_QhEnum;

enum class _Source_QvEnum : short {
	_VminusSYSTEM1,
	_VminusSYSTEM2,
	_VminusSYSTEM3,
	_VminusSYSTEM4,
	_VminusSYSTEM5,
} ;
typedef _Source_QvEnum Source_QvEnum;

/*----- PROTECTED REGION ID(TuneAdjust::Additional Class Declarations) ENABLED START -----*/

const size_t EBS_QUAD_NB        = 610;   // 512 + 2 + 96

/*----- PROTECTED REGION END -----*/	//	TuneAdjust::Additional Class Declarations

class TuneAdjust : public TANGO_BASE_CLASS
{

/*----- PROTECTED REGION ID(TuneAdjust::Data Members) ENABLED START -----*/

private:

  std::vector<double> mat;
  Tango::DeviceProxy *allQuadsDS;
  Tango::DeviceProxy *machstatDS;
  std::chrono::system_clock::time_point SetTune_cmd_date;

  Tango::AttributeProxy *att_h_tune;
  Tango::AttributeProxy *att_v_tune;

  std::vector<double> mem_strength;
  std::vector<double> strength_set_tune;

  std::chrono::system_clock::time_point h_tune_read_date;
  std::chrono::system_clock::time_point v_tune_read_date;

  void set_tune(std::vector<double> &);
  void read_matrix();
  void split(std::vector<std::string> &tokens, const std::string &text, char sep);
  void set_source_attribute(const std::string& attName,std::vector<std::string>& prop);

/*----- PROTECTED REGION END -----*/	//	TuneAdjust::Data Members

//	Device property data members
public:
	//	QuadDevName:	All quadrupole device name
	std::string	quadDevName;
	//	Delta_t_Qh:	Delta time (in mS) after SetTune command for H Tune alarm
	//  computation
	Tango::DevLong	delta_t_Qh;
	//	Delta_t_Qv:	Delta time (in mS) after SetTune command for V Tune alarm
	//  computation
	Tango::DevLong	delta_t_Qv;
	//	Delta_val_Qh:	Delta value threshold for H Tune alarm computation
	Tango::DevDouble	delta_val_Qh;
	//	Delta_val_Qv:	Delta value threshold for V Tune alarm computation
	Tango::DevDouble	delta_val_Qv;
	//	Source_Qh:	For each source system, a couple of values:
	//  - First the measurement system name
	//  - The Qh source attribute name (dev/att)
	std::vector<std::string>	source_Qh;
	//	Source_Qv:	For each source system, a couple of values:
	//  - First the measurement system name
	//  - The Qv source attribute name (dev/att)
	std::vector<std::string>	source_Qv;
	//	MaxDeltaTune:	Max value for delta tune in the ApplyDeltaTune command
	Tango::DevDouble	maxDeltaTune;
	//	MatrixFileName:	Matrix file name
	std::string	matrixFileName;
	//	MachstatDevName:	Name of the machstat device
	std::string	machstatDevName;

	bool	mandatoryNotDefined;

//	Attribute data members
public:
	Tango::DevDouble	*attr_Desired_Qh_read;
	Tango::DevDouble	*attr_Desired_Qv_read;
	Source_QhEnum	*attr_Source_Qh_read;
	Source_QvEnum	*attr_Source_Qv_read;
	Tango::DevDouble	*attr_Qh_read;
	Tango::DevDouble	*attr_Qv_read;
	Tango::DevDouble	*attr_ComputationMatrix_read;

//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	TuneAdjust(Tango::DeviceClass *cl,std::string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	TuneAdjust(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	TuneAdjust(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */
	~TuneAdjust();


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();

	/*
	 *	Check if mandatory property has been set
	 */
	 void check_mandatory_property(Tango::DbDatum &class_prop, Tango::DbDatum &dev_prop);

//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : TuneAdjust::read_attr_hardware()
	 * Description:  Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(std::vector<long> &attr_list);
	//--------------------------------------------------------
	/*
	 *	Method      : TuneAdjust::write_attr_hardware()
	 * Description:  Hardware writing for attributes.
	 */
	//--------------------------------------------------------
	virtual void write_attr_hardware(std::vector<long> &attr_list);

/**
 *	Attribute Desired_Qh related methods
 * Description:  The desired tune in H plane
 *
 *	Data type:  Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_Desired_Qh(Tango::Attribute &attr);
	virtual void write_Desired_Qh(Tango::WAttribute &attr);
	virtual bool is_Desired_Qh_allowed(Tango::AttReqType type);
/**
 *	Attribute Desired_Qv related methods
 * Description:  The desired tune in V plane
 *
 *	Data type:  Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_Desired_Qv(Tango::Attribute &attr);
	virtual void write_Desired_Qv(Tango::WAttribute &attr);
	virtual bool is_Desired_Qv_allowed(Tango::AttReqType type);
/**
 *	Attribute Source_Qh related methods
 *
 *
 *	Data type:  Tango::DevEnum
 *	Attr type:	Scalar
 */
	virtual void read_Source_Qh(Tango::Attribute &attr);
	virtual void write_Source_Qh(Tango::WAttribute &attr);
	virtual bool is_Source_Qh_allowed(Tango::AttReqType type);
/**
 *	Attribute Source_Qv related methods
 *
 *
 *	Data type:  Tango::DevEnum
 *	Attr type:	Scalar
 */
	virtual void read_Source_Qv(Tango::Attribute &attr);
	virtual void write_Source_Qv(Tango::WAttribute &attr);
	virtual bool is_Source_Qv_allowed(Tango::AttReqType type);
/**
 *	Attribute Qh related methods
 *
 *
 *	Data type:  Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_Qh(Tango::Attribute &attr);
	virtual bool is_Qh_allowed(Tango::AttReqType type);
/**
 *	Attribute Qv related methods
 *
 *
 *	Data type:  Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_Qv(Tango::Attribute &attr);
	virtual bool is_Qv_allowed(Tango::AttReqType type);
/**
 *	Attribute ComputationMatrix related methods
 * Description:  Matrix used to compute delta current from delta tune
 *
 *	Data type:  Tango::DevDouble
 *	Attr type:	Image max = 2 x 610
 */
	virtual void read_ComputationMatrix(Tango::Attribute &attr);
	virtual bool is_ComputationMatrix_allowed(Tango::AttReqType type);


	//--------------------------------------------------------
	/**
	 *	Method      : TuneAdjust::add_dynamic_attributes()
	 * Description:  Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();




//	Command related methods
public:
	/**
	 *	Command State related method
	 * Description:  This command gets the device state (stored in its device_state data member) and returns it to the caller.
	 *
	 *	@returns Device state
	 */
	virtual Tango::DevState dev_state();
	/**
	 *	Command Reset related method
	 *
	 *
	 */
	virtual void reset();
	virtual bool is_Reset_allowed(const CORBA::Any &any);
	/**
	 *	Command SetTune related method
	 * Description:  Compute and Apply delta current on quadrupole to set tunes
	 *               to desired values
	 *
	 */
	virtual void set_tune();
	virtual bool is_SetTune_allowed(const CORBA::Any &any);
	/**
	 *	Command MemorizeQuad related method
	 * Description:  Memorize quadrupoles values (for possible re-write with command
	 *               ResetQuadToMemorized)
	 *
	 */
	virtual void memorize_quad();
	virtual bool is_MemorizeQuad_allowed(const CORBA::Any &any);
	/**
	 *	Command ResetQuadToMemorized related method
	 * Description:  Reset qudrupoles to memorized values
	 *
	 */
	virtual void reset_quad_to_memorized();
	virtual bool is_ResetQuadToMemorized_allowed(const CORBA::Any &any);
	/**
	 *	Command ResetQuadPrevious related method
	 * Description:  Reset Quad PS to previous values (Values before last SetTune
	 *               command)
	 *
	 */
	virtual void reset_quad_previous();
	virtual bool is_ResetQuadPrevious_allowed(const CORBA::Any &any);
	/**
	 *	Command ApplyDeltaTune related method
	 * Description:  Apply given delta tune
	 *
	 *	@param argin in[0] = delta tune H
	 *	in[1] = delta tune V
	 */
	virtual void apply_delta_tune(const Tango::DevVarDoubleArray *argin);
	virtual bool is_ApplyDeltaTune_allowed(const CORBA::Any &any);


	//--------------------------------------------------------
	/**
	 *	Method      : TuneAdjust::add_dynamic_commands()
	 * Description:  Add dynamic commands if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_commands();

/*----- PROTECTED REGION ID(TuneAdjust::Additional Method prototypes) ENABLED START -----*/

/*----- PROTECTED REGION END -----*/	//	TuneAdjust::Additional Method prototypes
};

/*----- PROTECTED REGION ID(TuneAdjust::Additional Classes Definitions) ENABLED START -----*/

//	Additional Classes Definitions

/*----- PROTECTED REGION END -----*/	//	TuneAdjust::Additional Classes Definitions

}	//	End of namespace

#endif   //	TuneAdjust_H
