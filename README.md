# TuneAdjust

Do some small tune adjustments by changing quadrupole(s) strengths.

## Configuration

```
# Alarm settings (prevent from changing tune if measurement is too old)
# Enabled only if MachstatDevName (for reading machine current) is defined
Delta_t_Qh: 1000
Delta_t_Qv: 1000
Delta_val_Qh: 0.1
Delta_val_Qv: 0.1
MachstatDevName: sys/machstat/tango
```

```
# Tune response
MatrixFileName: "/operation/beamdyn/matlab/optics/sr/theory/tunemat.csv"
```

```
# Quad all
QuadDevName: "srmag/m-q/all"
```

```
# Source for tune measurement
Source_Qh: Simulator,"srdiag/beam-tune/1/Tune_h"
Source_Qv: Simulator,"srdiag/beam-tune/1/Tune_v"
```

## Cloning

To clone this project, type
```
git clone git@gitlab.esrf.fr:accelerators/BeamControl/TuneAdjust.git
```

## Documentation

Pogo generated doc in folder **doc_html**

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

#### Toolchain Dependencies

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build.

### Build Flags

Custom build flags for the project device server: 

| Flag | Default | Use |
|------|---------|-----|
|DRY_RUN|Not defined|If defined, no action done on imported device(s)|

### Build

Instructions on building the project.

CMake example:

```bash
cd TuneAdjust
mkdir build
cd build
cmake ../
make
```

## Tests

Some simple tests sequences in **test** folder
To build tests, you need [catch](https://github.com/catchorg/Catch2):
```bash
cd TuneAdjust/build
make test_tune_adjust
```
Binary file will be stored in TuneAdjust/bin folder
To run those tests:
* Use the acudebian7:10000 Tango host
* Start the device server with *manu* as instance name
* Run the tests

## CI (Continuous Integration)

This project is configured to use gitlab CI. After each git push, the
project is compiled and the tests are run.

[![coverage report](https://gitlab.esrf.fr/accelerators/TuneAdjust/badges/master/coverage.svg)](https://gitlab.esrf.fr/accelerators/TuneAdjust/commits/master)
[![build status](https://gitlab.esrf.fr/accelerators/TuneAdjust/badges/master/build.svg)](https://gitlab.esrf.fr/accelerators/TuneAdjust/commits/master)

See .gitlab-ci.yml file

