# Generic main project
project(Root)
cmake_minimum_required (VERSION 3.16)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")
include(lib-common)
include(lib-project)

###############################################################################
# DEFAULT PROJECT CONFIGURATION 

set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)
set(CMAKE_DISABLE_SOURCE_CHANGES  ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_COLOR_MAKEFILE ON)
set(CMAKE_SKIP_RPATH ON)

OPTION(BUILD_TESTING        "Build all the unit-tests"                  OFF)

# Manage common tasks
include(build-common)


###############################################################################
# Local Tango Classes build in this project
addTangoDevices(
    TANGO_CLASSES_STATIC
       TuneAdjust
    TANGO_SERVERS
       TuneAdjust
)
